""" Данный модуль является ведущем модулем. В его составе находится точка запуска
 всего приложения и основной код программы. """

# стандартные модули Python 3.8
import codecs
from os import path

# кастомные классы программы
from classes.user import User
# кастомные функции программы
from modules.sorts import (
    shaker_sort,
    bubble_sort
)
from modules.math import (
    arithmetic_avg,
    square_avg
)

ENCODING_LIST = [       #
    'utf-8',            # Разрешенные кодировки
    'windows-1251'      #
]                       #

PATH_TO_FILES = 'data/'                 # указываем рабочую папку
SRC_FILE_NAME = 'source_data.txt'       # Указываем имя файла, которое будем искать
RES_FILE_NAME = 'result.txt'            # Указываем желаемое имя выходного файла
ENCODING = ENCODING_LIST[0]             # глобальный доступ к рабочей кодировке (Указать нужную)


def read_user_from_file() -> User:
    """ Считывает первые 2 строки из файла SRC_FILE_NAME """
    try:
        f = codecs.open(PATH_TO_FILES + SRC_FILE_NAME, 'r', encoding=ENCODING)
        full_name = f.readline()
        identification = int(f.readline())

        # делим полное имя на составляющие
        name_elements = full_name.strip().split(' ')
        return User(name_elements[0], name_elements[1], name_elements[2], identification)
    except OSError:
        print(f'Файл {SRC_FILE_NAME} отсутствует в директории data')
        exit()


def create_user_file() -> None:
    """ создает файл с результатом работы  """
    f = codecs.open(PATH_TO_FILES + RES_FILE_NAME, 'w', encoding=ENCODING)
    f.close()


def record_user_in_file(user: User, direction: str, data_unicode: list, arith_avg: float, sqr_avg: float,
                        sorted_data: list) -> None:
    """ Создает запись результата в файле RES_FILE_NAME """
    if not path.exists(PATH_TO_FILES + RES_FILE_NAME):
        # создаем файл, если его не существует в рабочей директории
        create_user_file()

    # формируем список данных для записи в файл
    lines_for_write = []

    lines_for_write.append("Исходные данные:" + str(user.to_full_name(with_spaces=True)) + ": ID: "
                           + str(user.identification))
    lines_for_write.append(str(int(user.identification / 2)))
    if direction == 'inc':
        lines_for_write.append("Направление сортировки: по возрастанию, так как число "
                               + str(round(user.identification / 2)) + " чётное")
    else:
        lines_for_write.append("Направление сортировки: по убыванию, так как число "
                               + str(round(user.identification / 2)) + " не чётное")
    lines_for_write.append("Набор данных: " + str(data_unicode))
    if direction == 'inc':
        lines_for_write.append("Отсортированный по возрастанию набор данных " + str(sorted_data))
    else:
        lines_for_write.append("Отсортированный по убыванию набор данных " + str(sorted_data))
    lines_for_write.append("Среднее арифметическое значение: " + str(round(arith_avg)))
    lines_for_write.append("Среднее квадратическое значение: " + str(sqr_avg))

    # записываем сформированные строки в файл
    f = codecs.open(PATH_TO_FILES + RES_FILE_NAME, 'w', encoding=ENCODING)
    f.writelines("%s\n" % line for line in lines_for_write)
    f.close()


# основная функция которая будет работать
def main():
    """ Данная функция является главное функцией всего приложения, в ней происходит
     вызов других функций, создание переменных и работа над ними """
    # получаем данные о пользователе из файла
    user = read_user_from_file()
    # Выясняем направление сортировки
    direction = user.calc_direction()
    # получаем фио в стандарте unicode
    data_unicode = user.full_name_unicode()

    bubble_sort_result = []
    shaker_sort_result = []

    # производим сортировку по возрастанию или убыванию
    if direction == 'inc':
        bubble_sort_result = bubble_sort(data_unicode)
        shaker_sort_result = shaker_sort(data_unicode)
    else:
        bubble_sort_result = bubble_sort(data_unicode, reverse=True)
        shaker_sort_result = shaker_sort(data_unicode, reverse=True)

    # вычисляем среднее арифметическое и квадратическое значение
    arith_avg = arithmetic_avg(data_unicode)
    sqr_avg = square_avg(data_unicode)

    # записываем результат работы в файл
    record_user_in_file(user, direction, data_unicode, arith_avg, sqr_avg, bubble_sort_result)


# точка запуска основной функции
if __name__ == '__main__':
    main()
