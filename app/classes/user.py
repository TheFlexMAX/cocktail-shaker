""" Данные модуль предназначен для предоставления класса User, который отображает
 единственную запись в файле обработки данных
"""


class User:
    """ Данный класс отображает единствунную логическую запись из обрабатываемого
        файла
    """
    def __init__(self, last_name="", first_name="", middle_name="", identification=0):
        """ Инициализирует и объявляет свойства класса User"""
        self.last_name = last_name              # Фамилия
        self.first_name = first_name            # Имя
        self.middle_name = middle_name          # Отчества
        self.identification = identification    # ID пользователя

    # свойства и методы для фамилии
    @property
    def last_name(self):
        """ Объявление фамилии пользователя методом класса. """
        return self._last_name

    @last_name.setter
    def last_name(self, value: str):
        """ Setter фамилии класса User """
        self._last_name = value

    @last_name.getter
    def last_name(self):
        """ Getter фамилии класса User """
        return self._last_name

    # свойства и методы для имени
    @property
    def first_name(self):
        """ Объявление имени пользователя методом класса. """
        return self._first_name

    @first_name.setter
    def first_name(self, value: str):
        """ Setter имени класса User """
        self._first_name = value

    # свойства и методы для отчества
    @property
    def middle_name(self):
        """ Объявление отчества пользователя методом класса. """
        return self._middle_name

    @middle_name.setter
    def middle_name(self, value: str):
        """ Setter отчества класса User. """
        self._middle_name = value

    # свойства и методы для идентификатора
    @property
    def identification(self):
        """ Объявление идентификатора пользователя методом класса. """
        return self._identification

    @identification.setter
    def identification(self, value: int):
        """ Setter идентификатора класса User. """
        self._identification = value

    def calc_direction(self) -> str:
        """ данный метод вычисляем направления будущей сортировки данных
         класса User
        """
        full_name_len = len(self.to_full_name())
        calc_value = round(self.identification / full_name_len)
        if calc_value % 2 == 0:
            return 'inc'
        return 'dec'

    def to_full_name(self, with_spaces=False) -> str:
        """ Производит конкатенацию строк имени, фамилии и отчества """
        if with_spaces:
            return self.last_name + " " + self.first_name + " " + self.middle_name
        return self.last_name + self.first_name + self.middle_name

    def full_name_unicode(self) -> list:
        """ Переводит фамилию, имя и отчество в символы ascii в десятичном
         формате
        """
        return list(ord(line) for line in self.to_full_name())
