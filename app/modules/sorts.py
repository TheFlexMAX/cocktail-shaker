""" Предоставляет набор функций сортировки списков """


# Алгоритм сортировки по методу пузырька
def bubble_sort(arr: list, reverse=False) -> list:
    """ Функция сортирующая массив методом 'пузырька' """
    arr_len = len(arr) - 1
    arr_copy = arr.copy()

    # Сортировка по убыванию
    if reverse:
        for i in range(arr_len):
            for j in range(arr_len, 0, -1):     # третьим аргументом сообщаем об обратном шаге цикла как i--
                if arr_copy[j] > arr_copy[j - 1]:
                    arr_copy[j], arr_copy[j - 1] = arr_copy[j - 1], arr_copy[j]
    # Сортировка по возрастанию
    else:
        for i in range(arr_len):
            for j in range(arr_len - i):
                if arr_copy[j] > arr_copy[j + 1]:
                    arr_copy[j], arr_copy[j + 1] = arr_copy[j + 1], arr_copy[j]

    return arr_copy


# алгоритм "Шейкерная сортировка"
def shaker_sort(arr: list, reverse=False) -> list:
    """ Функция сортирующая массив методом 'перемешивания' """
    left = 0
    right = len(arr) - 1
    arr_copy = arr.copy()

    # Сортировка по убыванию
    if reverse:
        while left <= right:
            for i in range(left, right, +1):
                if arr_copy[i] < arr_copy[i + 1]:
                    arr_copy[i], arr_copy[i + 1] = arr_copy[i + 1], arr_copy[i]
            right -= 1

            for i in range(right, left, -1):
                if arr_copy[i - 1] < arr_copy[i]:
                    arr_copy[i], arr_copy[i - 1] = arr_copy[i - 1], arr_copy[i]
            left += 1
    # Сортировка по возрастанию
    else:
        while left <= right:
            for i in range(left, right, +1):
                if arr_copy[i] > arr_copy[i + 1]:
                    arr_copy[i], arr_copy[i + 1] = arr_copy[i + 1], arr_copy[i]
            right -= 1

            for i in range(right, left, -1):
                if arr_copy[i - 1] > arr_copy[i]:
                    arr_copy[i], arr_copy[i - 1] = arr_copy[i - 1], arr_copy[i]
            left += 1

    return arr_copy
