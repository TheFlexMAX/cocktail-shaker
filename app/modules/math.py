""" Данный файл предоставляет набор функций для вычисления средних значений
 численных списков данных. """
from math import sqrt


def arithmetic_avg(arr: list) -> float:
    """ Вычисляет среднее арифметическое значение из списка чисел """
    arr_sum = 0

    for num in arr:
        arr_sum += num

    return round(arr_sum / len(arr), 3)


def square_avg(arr: list) -> float:
    """ Вычисляет среднее квадратичное значение из списка чисел """
    arr_sum_sqr = 0

    for num in arr:
        arr_sum_sqr += pow(num, 2)

    return round(sqrt(arr_sum_sqr / len(arr)), 3)
